﻿namespace RedditPhotoParser
{
	partial class FMain
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FMain));
			this.PanelA = new System.Windows.Forms.Panel();
			this.VersionInfoL = new System.Windows.Forms.Label();
			this.StatusL = new System.Windows.Forms.Label();
			this.WebAddressTB = new System.Windows.Forms.TextBox();
			this.MenuPanel = new System.Windows.Forms.Panel();
			this.PanelB = new System.Windows.Forms.Panel();
			this.StatusT = new System.Windows.Forms.Timer(this.components);
			this.PanelA.SuspendLayout();
			this.SuspendLayout();
			// 
			// PanelA
			// 
			this.PanelA.Controls.Add(this.VersionInfoL);
			this.PanelA.Controls.Add(this.StatusL);
			this.PanelA.Controls.Add(this.WebAddressTB);
			this.PanelA.Controls.Add(this.MenuPanel);
			this.PanelA.Location = new System.Drawing.Point(24, 12);
			this.PanelA.Name = "PanelA";
			this.PanelA.Size = new System.Drawing.Size(603, 109);
			this.PanelA.TabIndex = 0;
			// 
			// VersionInfoL
			// 
			this.VersionInfoL.AutoSize = true;
			this.VersionInfoL.Font = new System.Drawing.Font("Segoe UI Semilight", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.VersionInfoL.ForeColor = System.Drawing.Color.White;
			this.VersionInfoL.Location = new System.Drawing.Point(209, 80);
			this.VersionInfoL.Name = "VersionInfoL";
			this.VersionInfoL.Size = new System.Drawing.Size(273, 19);
			this.VersionInfoL.TabIndex = 3;
			this.VersionInfoL.Text = "by BogdanBBA - v1.0 (August 7th-8th, 2014)";
			// 
			// StatusL
			// 
			this.StatusL.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.StatusL.ForeColor = System.Drawing.Color.White;
			this.StatusL.Location = new System.Drawing.Point(11, 55);
			this.StatusL.Name = "StatusL";
			this.StatusL.Size = new System.Drawing.Size(237, 32);
			this.StatusL.TabIndex = 2;
			this.StatusL.Text = "Awaiting your command, mon capitaine!";
			// 
			// WebAddressTB
			// 
			this.WebAddressTB.BackColor = System.Drawing.SystemColors.WindowFrame;
			this.WebAddressTB.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.WebAddressTB.Font = new System.Drawing.Font("Segoe UI Light", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.WebAddressTB.ForeColor = System.Drawing.Color.DeepSkyBlue;
			this.WebAddressTB.Location = new System.Drawing.Point(16, 16);
			this.WebAddressTB.Name = "WebAddressTB";
			this.WebAddressTB.Size = new System.Drawing.Size(351, 36);
			this.WebAddressTB.TabIndex = 1;
			this.WebAddressTB.Text = "http://www.reddit.com/r/MapPorn/.rss";
			// 
			// MenuPanel
			// 
			this.MenuPanel.Location = new System.Drawing.Point(439, 16);
			this.MenuPanel.Name = "MenuPanel";
			this.MenuPanel.Size = new System.Drawing.Size(200, 58);
			this.MenuPanel.TabIndex = 0;
			// 
			// PanelB
			// 
			this.PanelB.AutoScroll = true;
			this.PanelB.Location = new System.Drawing.Point(36, 161);
			this.PanelB.Name = "PanelB";
			this.PanelB.Size = new System.Drawing.Size(603, 109);
			this.PanelB.TabIndex = 1;
			// 
			// StatusT
			// 
			this.StatusT.Tick += new System.EventHandler(this.StatusT_Tick);
			// 
			// FMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ControlDark;
			this.ClientSize = new System.Drawing.Size(675, 431);
			this.ControlBox = false;
			this.Controls.Add(this.PanelB);
			this.Controls.Add(this.PanelA);
			this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ForeColor = System.Drawing.Color.DimGray;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
			this.Name = "FMain";
			this.Text = "Reddit photo parser";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.Shown += new System.EventHandler(this.FMain_Shown);
			this.Resize += new System.EventHandler(this.FMain_Resize);
			this.PanelA.ResumeLayout(false);
			this.PanelA.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel PanelA;
		private System.Windows.Forms.Panel MenuPanel;
		private System.Windows.Forms.Panel PanelB;
		private System.Windows.Forms.TextBox WebAddressTB;
		private System.Windows.Forms.Label StatusL;
		private System.Windows.Forms.Timer StatusT;
		private System.Windows.Forms.Label VersionInfoL;
	}
}

