﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RssToolkit;
using RedditPhotoParser.Classes;
using System.Xml;

namespace RedditPhotoParser
{
	public partial class FMain : Form
	{
		public const int ComponentPadding = 16;
		public const int PhotoColumns = 4;
		public const int PhotoRows = 2;
		public const int StatusTimerInterval = 500;
		public const string WorkspacePath = "workspace";

		private static List<string> MenuButtonCaptions = new List<string>(new string[3] { "Parse!", "Open workspace", "Exit" });

		private static Color StatusNormalC = ColorTranslator.FromHtml("#EEEEEE");
		private static Color StatusWorkingC = ColorTranslator.FromHtml("#999999");
		private static Color StatusErrorC = ColorTranslator.FromHtml("#DD0000");

		private List<PhotoDownloader> Photos = new List<PhotoDownloader>();
		private int PhotoStartIndex;

		public FMain()
		{
			InitializeComponent();
			MyGuiComponents.InitializeAndFormatFormComponents(this);
			if (!Directory.Exists(WorkspacePath))
				Directory.CreateDirectory(WorkspacePath);
		}

		private void FMain_Resize(object sender, EventArgs e)
		{
			PanelA.SetBounds(ComponentPadding, ComponentPadding, this.Width - 2 * ComponentPadding, 100);
			MenuPanel.SetBounds(PanelA.Width - MenuButtonCaptions.Count * 200, 0, MenuButtonCaptions.Count * 200, 60);
			WebAddressTB.SetBounds(ComponentPadding, ComponentPadding, MenuPanel.Left - 2 * ComponentPadding, WebAddressTB.Height);
			StatusL.SetBounds(WebAddressTB.Left, WebAddressTB.Bottom + 8, WebAddressTB.Width, 32);
			VersionInfoL.Location = new Point(PanelA.Width - VersionInfoL.Width, StatusL.Bottom - VersionInfoL.Height);
			PanelB.SetBounds(ComponentPadding, PanelA.Bottom + ComponentPadding, this.Width - ComponentPadding, this.Height - PanelB.Top - ComponentPadding);
		}

		private void FMain_Shown(object sender, EventArgs e)
		{
			MyGuiComponents.CreateMenuButtons(MenuPanel, MenuButtonCaptions, true, MenuButton_Click);
			WebAddressTB.SelectionStart = WebAddressTB.Text.Length;
		}

		/// 
		/// 
		/// 

		private void MenuButton_Click(object sender, EventArgs e)
		{
			int r = MenuButtonCaptions.IndexOf(((PictureBoxButton) sender).Information);
			switch (r)
			{
				case 0: // parse
					try
					{
						SetStatus("Initializing...", StatusWorkingC);
						foreach (PhotoDownloader pd in Photos)
							pd.GetThumbnailImage().Hide();
						Photos.Clear();
						Photos.TrimExcess();
						string src = WorkspacePath + "\\index.xml";
						if (File.Exists(src))
							File.Delete(src);
						StatusT.Interval = StatusTimerInterval;

						SetStatus("Downloading RSS...", StatusWorkingC);
						File.WriteAllText(src, RssToolkit.Rss.RssDocument.Load(new Uri(WebAddressTB.Text)).ToXml(RssToolkit.Rss.DocumentType.Rss), Encoding.Unicode);

						SetStatus("Preparing controls...", StatusWorkingC);
						Size size = new Size((PanelB.Width - (PhotoColumns - 1) * ComponentPadding - System.Windows.Forms.SystemInformation.HorizontalScrollBarHeight) / PhotoColumns, 
                                 (PanelB.Height - (PhotoRows - 1) * ComponentPadding - System.Windows.Forms.SystemInformation.VerticalScrollBarWidth) / PhotoRows);
						int lastX = 0, lastY = 0;

						SetStatus("Parsing RSS...", StatusWorkingC);
						XmlDocument doc = new XmlDocument();
						doc.Load(src);
						foreach (XmlNode i in doc.SelectNodes("rss/channel/item"))
							try
							{
								string url, title = i.SelectSingleNode("title").InnerText, descr = i.SelectSingleNode("description").InnerText;
								RssItemInfo info = new RssItemInfo(title, PhotoDownloader.GetImgurURL(descr, out url) ? url : "");
								PhotoDownloader pd = new PhotoDownloader(info, PanelB, new Rectangle(lastX, lastY, size.Width, size.Height));
								Photos.Add(pd);
								lastX += size.Width + ComponentPadding;
								if (lastX >= PanelB.Width - 10)
								{
									lastX = 0;
									lastY += size.Height + ComponentPadding;
								}
							}
							finally { }

						SetStatus("Downloading photos...", StatusWorkingC);
						PhotoStartIndex = 0;
						StatusT.Enabled = true;
					}
					finally
					{

					}
					break;
				case 1: // open workspace
					System.Diagnostics.Process.Start(WorkspacePath);
					break;
				case 2: // exit
					if (GetNumberOfWorkingThreads() != 0)
						MessageBox.Show("You can't exit while the download is in progress.", "Be pacient*", MessageBoxButtons.OK, MessageBoxIcon.Information);
					else
					{
						foreach (PhotoDownloader pd in Photos)
							pd.StopThread();
						Application.Exit();
					}
					break;
			}
		}

		///
		///
		///

		private void StatusT_Tick(object sender, EventArgs e)
		{
			if (PhotoStartIndex < Photos.Count)
			{
				Photos[PhotoStartIndex].StartThread();
				PhotoStartIndex++;
			}
			else
			{
				if (GetNumberOfWorkingThreads() == 0)
				{
					SetStatus(string.Format("Operation finished ({0}/{1} pics downloaded). You're awesome though!",
						GetNumberOfThreadsInPhase(PhotoDownloader.PhaseSuccess), Photos.Count), StatusNormalC);
					StatusT.Enabled = false;
				}
			}
		}

		private void SetStatus(string text, Color color)
		{
			StatusL.Text = text;
			StatusL.ForeColor = color;
		}

		private int GetNumberOfWorkingThreads()
		{
			return GetNumberOfThreadsInPhase(PhotoDownloader.PhaseWorking);
		}

		private int GetNumberOfThreadsInPhase(int phase)
		{
			int nInPhase = 0;
			foreach (PhotoDownloader pd in Photos)
				if (pd.Phase() == phase)
					nInPhase++;
			return nInPhase;
		}
	}
}
