﻿using SimpleFeedReader;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;

namespace RedditPhotoParser.Classes
{
	public struct RssItemInfo
	{
		public string Title; // the title on reddit
		public string ImageURL; // empty string if extraction failed
		public string ImagePath; // empty string if download failed

		public RssItemInfo(string Title, string ImageURL)
		{
			this.Title = Title;
			this.ImageURL = ImageURL;
			this.ImagePath = "";
		}
	}

	public class PhotoDownloader
	{
		public const int PhasePending = 0;
		public const int PhaseWorking = 1;
		public const int PhaseSuccess = 2;
		public const int PhaseFailed = 3;
		private const int ThumbHeaderHeight = 32;

		private static Brush BgBrush = new SolidBrush(MyGuiComponents.ButtonC);
		private static Brush BgBrushMO = new SolidBrush(MyGuiComponents.SelectedButtonC);
		private static Brush TextBrush = new SolidBrush(MyGuiComponents.FontC);
		private static Brush TextBrushMO = new SolidBrush(MyGuiComponents.SelectedFontC);
		private static Font TextFont = MyGuiComponents.GetFont("Segoe UI", 10, true);
		private static Font FailFont = MyGuiComponents.GetFont("Segoe UI", 50, true);

		private RssItemInfo Info; // item info
		private Image SmallPhoto; // a small version if the downloaded pic, when ready
		private PictureBox Thumb; // the image control on the main form
		private BackgroundWorker DownloadThread; // download and resize thread

		private int CurrentPhase;
		private string ErrorMessage;

		public PhotoDownloader(RssItemInfo Info, Panel container, Rectangle bounds)
		{
			this.Info = Info;
			this.SmallPhoto = null;
			this.CurrentPhase = PhasePending;
			this.ErrorMessage = "(between you and me, you shouldn't be reading this; something's weirdly wrong)";

			this.Thumb = new PictureBox();
			this.Thumb.Parent = container;
			this.Thumb.SetBounds(bounds.Left, bounds.Top, bounds.Width, bounds.Height);
			this.Thumb.Click += new EventHandler(Thumb_Click);
			this.Thumb.MouseEnter += new EventHandler(Thumb_MouseEnter);
			this.Thumb.MouseLeave += new EventHandler(Thumb_MouseLeave);

			this.DownloadThread = new BackgroundWorker();
			this.DownloadThread.WorkerReportsProgress = true;
			this.DownloadThread.WorkerSupportsCancellation = true;
			this.DownloadThread.DoWork += new DoWorkEventHandler(Thread_WORK);
			this.DownloadThread.ProgressChanged += new ProgressChangedEventHandler(Thread_ProgressChanged);
			this.DownloadThread.RunWorkerCompleted += new RunWorkerCompletedEventHandler(Thread_WorkComplete);

			this.RedrawThumb(false);
		}

		// Interfacing

		public void StartThread()
		{
			if (!DownloadThread.IsBusy)
				DownloadThread.RunWorkerAsync();
		}

		public void StopThread()
		{
			if (DownloadThread.IsBusy)
				DownloadThread.CancelAsync();
		}

		public int Phase()
		{
			return CurrentPhase;
		}

		public PictureBox GetThumbnailImage()
		{
			return Thumb;
		}

		// Thread-related

		private void Thread_WORK(object sender, EventArgs e)
		{
			CurrentPhase = PhaseWorking;
			Thread_ProgressChanged(null, null);
			string filename;

			bool imageDownloadedSuccessfully = DownloadFileFromTheInternet(new Uri(Info.ImageURL));

			if (imageDownloadedSuccessfully)
				try
				{
					Image img = new Bitmap(Info.ImagePath);
					SmallPhoto = ScaleImage(img, Thumb.Width, Thumb.Height - ThumbHeaderHeight);
					img.Dispose();
					CurrentPhase = PhaseSuccess;
				}
				catch (Exception E)
				{
					ErrorMessage = E.ToString();
					CurrentPhase = PhaseFailed;
				}
			else
				CurrentPhase = PhaseFailed;
		}

		private bool DownloadFileFromTheInternet(Uri uri)
		{
			string filename;
			// is it a proper image?
			if (Regex.Match(Info.ImageURL, @".+\.(jpg)|(jpeg)|(png)", RegexOptions.IgnoreCase).Success)
				try
				{
					filename = FMain.WorkspacePath + "\\" + uri.LocalPath;
					new WebClient().DownloadFile(uri.OriginalString, filename);
					Info.ImagePath = filename;
					return true;
				}
				catch (Exception E)
				{
					filename = "";
					ErrorMessage = E.ToString();
					return false;
				}
			// it's a link to a webpage
			else
			{
				filename = "";
				ErrorMessage = "It's a link to a webpage rather than an actual image (but this is an upcoming feature, don't worry :) !).";
				return false;
			}
		}

		private static Image ScaleImage(Image image, int maxWidth, int maxHeight)
		{
			var ratioX = (double) maxWidth / image.Width;
			var ratioY = (double) maxHeight / image.Height;
			var ratio = Math.Min(ratioX, ratioY);

			var newWidth = (int) (image.Width * ratio);
			var newHeight = (int) (image.Height * ratio);

			var newImage = new Bitmap(newWidth, newHeight);
			Graphics.FromImage(newImage).DrawImage(image, 0, 0, newWidth, newHeight);
			return newImage;
		}

		private void Thread_ProgressChanged(object sender, EventArgs e)
		{
			RedrawThumb(false);
		}

		private void Thread_WorkComplete(object sender, EventArgs e)
		{
			Thumb.Cursor = CurrentPhase == PhaseSuccess || CurrentPhase == PhaseFailed ? Cursors.Hand : Cursors.Default;
			Thread_ProgressChanged(null, null);
		}

		// Thumb-related

		private void Thumb_Click(object sender, EventArgs e)
		{
			switch (CurrentPhase)
			{
				case PhaseSuccess: // finished successfully
					System.Diagnostics.Process.Start(Info.ImagePath);
					break;
				case PhaseFailed: // finished incorrectly
					MessageBox.Show(ErrorMessage, "Error message", MessageBoxButtons.OK);
					break;
			}
		}

		private void Thumb_MouseEnter(object sender, EventArgs e)
		{
			if (CurrentPhase >= PhaseSuccess)
				RedrawThumb(true);
		}

		private void Thumb_MouseLeave(object sender, EventArgs e)
		{
			if (CurrentPhase >= PhaseSuccess)
				RedrawThumb(false);
		}

		private void RedrawThumb(bool MouseOver)
		{
			if (Thumb.Image != null)
				Thumb.Image.Dispose();
			Bitmap bmp = new Bitmap(Thumb.Width, Thumb.Height);
			Graphics g = Graphics.FromImage(bmp);
			Brush bgBrush = MouseOver ? BgBrushMO : BgBrush;
			Brush textBrush = MouseOver ? TextBrushMO : TextBrush;
			g.FillRectangle(bgBrush, 0, 0, Thumb.Width, Thumb.Height);
			int x = bmp.Width / 2, y = bmp.Height / 2, size = 32;

			switch (CurrentPhase)
			{
				case PhasePending: // pending
					g.DrawString("Preparing to download...", TextFont, textBrush, new Point(4, 4));
					for (int i = -1; i <= 1; i++)
						g.FillEllipse(BgBrushMO, x + i * (size + size / 2) - size / 2, y - size / 2, size, size);
					break;
				case PhaseWorking: // working
					g.DrawString("Downloading \"" + Info.ImageURL + "\"...", TextFont, textBrush, new Point(4, 4));
					for (int i = -1; i <= 1; i++)
						g.FillEllipse(BgBrushMO, x + i * (size + size / 2) - size / 2, y - size / 2, size, size);
					break;
				case PhaseSuccess: // finished successfully
					g.DrawString(Info.Title, TextFont, textBrush, new Point(4, 4));
					int h2 = (Thumb.Height - ThumbHeaderHeight) / 2;
					g.DrawImage(SmallPhoto, new Point(Thumb.Width / 2 - SmallPhoto.Width / 2, ThumbHeaderHeight + h2 - SmallPhoto.Height / 2));
					break;
				case PhaseFailed: // finished incorrectly
					Size strSz = g.MeasureString(":(", FailFont).ToSize();
					g.DrawString(":(", FailFont, textBrush, new Point(x - strSz.Width / 2, y - strSz.Height / 2));
					break;
			}

			Thumb.Image = bmp;
		}

		// Other

		public static bool GetImgurURL(string content, out string url)
		{
			url = "";
			try
			{
				int p0 = content.IndexOf("[link]"), p = p0, nq = 0;
				while (nq < 2)
				{
					if (content[p] == '\"')
						nq++;
					p--;
				}
				url = content.Substring(p + 2, p0 - p - 4);
			}
			catch (Exception E) { return false; }
			return true;
		}
	}
}
