﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RedditPhotoParser.Classes
{
	public static class MyGuiComponents
	{
		public static Color FormBackgroundC = ColorTranslator.FromHtml("#222222");
		public static Color ButtonC = ColorTranslator.FromHtml("#222222");
		public static Color SelectedButtonC = ColorTranslator.FromHtml("#333333");
		public static Color FontC = ColorTranslator.FromHtml("#FFFFFF");
		public static Color SelectedFontC = ColorTranslator.FromHtml("#FFFFFF");

		public static Font GetFont(string name, int size, bool bold)
		{
			return new Font(name, size, bold ? FontStyle.Bold : FontStyle.Regular);
		}

		public static void InitializeAndFormatFormComponents(Form form)
		{
			form.BackColor = FormBackgroundC;

			foreach (Control ctrl in form.Controls)
				if (ctrl is Label)
					((Label) ctrl).BackColor = form.BackColor;
				else if (ctrl is Panel)
					((Panel) ctrl).BackColor = form.BackColor;
				else if (ctrl is PictureBox)
					((PictureBox) ctrl).BackColor = form.BackColor;
		}

		public static List<PictureBoxButton> CreateMenuButtons(Panel container, List<string> captions, bool horizontal, EventHandler click)
		{
			List<PictureBoxButton> result = new List<PictureBoxButton>();
			for (int i = 0, n = captions.Count, dim = horizontal ? container.Width / n : container.Height / n; i < n; i++)
			{
				PictureBoxButton pic = new PictureBoxButton(captions[i], click);
				pic.Parent = container;
				pic.Cursor = Cursors.Hand;
				if (horizontal)
					pic.SetBounds(i * dim, 0, dim, container.Height);
				else
					pic.SetBounds(0, i * dim, container.Width, dim);
				PictureBoxButton.OnMouseLeave(pic, null);
				result.Add(pic);
			}
			return result;
		}
	}

	/// 
	/// 
	/// 
	public class PictureBoxButton : PictureBox
	{
		public string Information;

		public PictureBoxButton(string Information, EventHandler Click)
			: this(Information, Click, OnMouseEnter, OnMouseLeave)
		{
		}

		public PictureBoxButton(string Information, EventHandler Click, EventHandler MouseEnter, EventHandler MouseLeave)
			: base()
		{
			this.Information = Information;
			this.Click += Click;
			this.MouseEnter += MouseEnter;
			this.MouseLeave += MouseLeave;
			MouseLeave(this, null);
		}

		public static void OnMouseEnter(object sender, EventArgs e)
		{
			DrawPictureBoxButton((PictureBoxButton) sender, true);
		}

		public static void OnMouseLeave(object sender, EventArgs e)
		{
			DrawPictureBoxButton((PictureBoxButton) sender, false);
		}

		private static void DrawPictureBoxButton(PictureBoxButton pic, bool selected)
		{
			if (pic.Image != null)
				pic.Image.Dispose();
			Bitmap bmp = new Bitmap(pic.Width, pic.Height);
			Graphics g = Graphics.FromImage(bmp);
			Brush bgBrush = selected ? new SolidBrush(MyGuiComponents.SelectedButtonC) : new SolidBrush(MyGuiComponents.ButtonC);
			Brush textBrush = selected ? new SolidBrush(MyGuiComponents.FontC) : new SolidBrush(MyGuiComponents.SelectedFontC);
			Font font = MyGuiComponents.GetFont("Segoe UI", 17, true);
			g.FillRectangle(bgBrush, 0, 0, pic.Width, pic.Height);
			if (selected)
				g.DrawRectangle(new Pen(Color.WhiteSmoke), 0, 0, pic.Width - 1, pic.Height - 1);
			Size size = g.MeasureString(pic.Information, font).ToSize();
			g.DrawString(pic.Information, font, textBrush, new Point(pic.Width / 2 - size.Width / 2, pic.Height / 2 - size.Height / 2));
			pic.Image = bmp;
		}
	}
}
